# amps-config

Scripts for AMPS setup and configuration

### Repositories

* AMPS web interface: https://gitlab.com/klappscheinwerfer/amps
* AMPS config: https://gitlab.com/klappscheinwerfer/amps-config
* Haskijs: https://gitlab.com/ELipiniks/haskijs

## Installation (debian)

* Install git `apt install git`
* Make a directory and move to it, for example `mkdir Repos; cd Repos`
* Clone this repository `git clone https://gitlab.com/klappscheinwerfer/amps-config`
* Uncomment lines for the components you want to install in `setup.sh`
	* Make sure the check the individual scripts before running them
* Run `setup.sh` as root

## Structure

<!-- Generated using 'tree' command -->
```
amps-conf
├── CONTRIBUTING.md (Style guidelines)
├── files
│   ├── ampsdb (Subdirectory for database related files)
│   │   ├── ampsdb_get.py (Script for creating a blacklist file for squid from the DB)
│   │   ├── ampsdb_put.py (Script for downloading site lists and inserting them into DB)
│   │   ├── cron (Cron file for running ampsdb_get.py and ampsdb_put.py automatically)
│   │   ├── makedb.sql (Creates database schema and adds some initial records)
│   │   ├── queries.txt (Queries for testing)
│   │   └── README.md
│   ├── haproxy (Subdirectory for haproxy related files)
│   │   ├── haproxy.cfg
│   │   ├── keepalived.conf.backup
│   │   └── keepalived.conf.master
│   ├── krb5.conf
│   ├── legacy (Subdirectory currently unused files that could become useful in the future)
│   │   ├── auto_download.sh (automatic blacklist download from shallalist)
│   │   ├── README.md
│   │   └── squid.conf (haskijs squid config file)
│   └── squid.conf
├── README.md
├── scripts (Scripts for setting up AMPS components)
│   ├── apache-setup.sh
│   ├── db-setup.sh
│   ├── haproxy-setup.sh
│   ├── squid-setup.sh
│   └── tools-setup.sh (Convenience script, not required)
└── setup.sh (Main script for setting up amps)
```

## Logging

* ampsdb_get.py: `/tmp/ampsdb_get.log`
* ampsdb_put.py: `/tmp/ampsdb_put.log`

## Useful
<!-- Some possibly useful resources -->

* Squid
	* https://wiki.squid-cache.org/ConfigExamles
	* https://wiki.squid-cache.org/ConfigExamles/Authenticate/Groups

* Virtual IP
	* https://help.hcltechsw.com/onetest/hclonetestapi/10.1.3/com.ibm.rational.rit.protocol.doc/topics/t_rit_virt_ip_config_lin.html
		* ip address add \<address> dev \<device>
		* ip address del \<address> dev \<device>
