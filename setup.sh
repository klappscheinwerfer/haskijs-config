#!/bin/bash

cd "${BASH_SOURCE/*}" || exit

# Check, if run as root

if [[ $EUID > 0 ]]
	then echo "Please run as root"
	exit
fi

# Update system

apt-get update
apt-get upgrade

# Run scripts

# NOTE: Uncomment the line which you intend to run
# NOTE: Check the scripts before running them

# source ./scripts/tools-setup.sh
# source ./scripts/squid-setup.sh
# source ./scripts/apache-setup.sh
# source ./scripts/db-setup.sh
# source ./scripts/cluster-setup.sh
# source ./scripts/cluster-main-setup.sh
# source ./scripts/haproxy-setup.sh
