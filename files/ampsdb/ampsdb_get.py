# Requirements: psycopg2

# IMPORTANT: Create the DB and populate it before running this script
# It is done automatically created with the 'db-setup.sh' script

import psycopg2
import psycopg2.extras
import time

# Start timer
start = time.time()

# Return a rounded time difference between the start of the program and now
def deltatime():
	return "T: " + "%.3f" % round(time.time() - start, 3) + "\t"

# Connect to DB
print("{}Connecting to database".format(deltatime()))
conn = psycopg2.connect(
	database = "ampsdb",
	user = "pguser",
	host= "localhost",
	port = 5432,
	password = "pass"
)
cursor = conn.cursor()
# Get lists
cursor.execute("SELECT * FROM list;")
lists = cursor.fetchall()

# Export blocklist
# TODO: take priority into account
print("{}Exporting blacklist".format(deltatime()))
with open("domain_blacklist.txt", 'w') as output:
	cursor.copy_expert("""
		COPY (
			SELECT url_text
			FROM Url
			INNER JOIN List
				ON Url.fk_list_id = List.list_id
			WHERE List.list_priority < 0
		) TO STDOUT
	""", output)

print("{}Closing the connection".format(deltatime()))
conn.close()