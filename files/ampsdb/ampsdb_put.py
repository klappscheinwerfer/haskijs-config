# Requirements: psycopg2

# IMPORTANT: Create the DB before running this script (see makedb.sql)
# Database is automatically created with the 'db-setup.sh' script

import psycopg2
import psycopg2.extras
import tarfile
import urllib.request
import time
import io

# Start timer
start = time.time()

# Return a rounded time difference between the start of the program and now
def deltatime():
	return "T: " + "%.3f" % round(time.time() - start, 3) + "\t"

# Connect to DB
print("{}Connecting to database".format(deltatime()))
conn = psycopg2.connect(
	database = "ampsdb",
	user = "pguser",
	host= "localhost",
	port = 5432,
	password = "pass"
)
cursor = conn.cursor()
# Get the id of 'default' list
cursor.execute("SELECT list_id FROM List WHERE list_name = 'default';")
default_list_id = cursor.fetchone()[0]
# Get the id of 'automatic' tag
cursor.execute("SELECT tag_id FROM Tag WHERE tag_name = 'automatic';")
automatic_tag_id = cursor.fetchone()[0]

# Create temporary tables
print("{}Creating temporary tables".format(deltatime()))
cursor.execute("""
		DROP TABLE IF EXISTS Temp_url;
		CREATE TABLE Temp_url (
			t_url_id SERIAL,
			t_url_text TEXT NOT NULL,
			t_url_reason TEXT NOT NULL,
			t_fk_list_id INTEGER NOT NULL
		);
		DROP TABLE IF EXISTS Temp_assignedtag;
		CREATE TABLE Temp_assignedtag (
			t_assignedtag_id SERIAL,
			t_fk_tag_id INTEGER NOT NULL,
			t_fk_url_id INTEGER NOT NULL
		);
	""")

# Download archive
print("{}Downloading blacklists".format(deltatime()))
urllib.request.urlretrieve(
	"ftp://ftp.ut-capitole.fr/pub/reseau/cache/squidguard_contrib/blacklists.tar.gz",
	"blacklists.tar.gz"
)

# Extract archive
print("{}Extracting blacklists".format(deltatime()))
tar = tarfile.open("blacklists.tar.gz", "r:gz")
for member in tar.getmembers():
	# Ignore everything except domains
	path = member.name.split('/')
	if "domains" not in path: continue
	category = path[1]
	print("{}Copying {}".format(deltatime(), category))

	# Check if tag exists, if not, create it
	cursor.execute("SELECT tag_id FROM Tag WHERE tag_name = '{}';".format(category))
	category_tag_id = cursor.fetchone()
	if category_tag_id == None:
		cursor.execute("INSERT INTO Tag (tag_name) VALUES ('{}') RETURNING tag_id;".format(category))
		category_tag_id = cursor.fetchone()
	category_tag_id = category_tag_id[0]

	# Extract file
	f = tar.extractfile(member)
	# Skip if file is empty
	if f is None: continue
	
	# Copy urls
	url_buffer = io.StringIO()
	for line in f.readlines():
		url_buffer.write('|'.join((
			line.decode().strip(),
			"Automatically added from ut-capitole.fr",
			str(default_list_id))) + '\n')
	url_buffer.seek(0)
	cursor.copy_from(url_buffer, "temp_url", sep='|', columns=("t_url_text", "t_url_reason", "t_fk_list_id"))
	del f

	# Merge Temp_url into Url
	print("{}Merging {}".format(deltatime(), category))
	cursor.execute("""
			INSERT INTO Url (url_text, url_reason, fk_list_id)
			SELECT t_url_text, t_url_reason, t_fk_list_id FROM Temp_url
			ON CONFLICT DO NOTHING
			RETURNING url_id;
		""")
	changed_tags = cursor.fetchall()
	cursor.execute("DELETE FROM Temp_url;")

	# Copy assigned tags
	if not changed_tags: continue
	print("{}Copying tags for {}".format(deltatime(), category))
	at_buffer = io.StringIO() # Assigned tag buffer
	for tag in changed_tags:
		# Automatic tag
		at_buffer.write('|'.join((
				str(automatic_tag_id),
				str(tag[0])
			))+ '\n')
		# Category tag
		at_buffer.write('|'.join((
				str(category_tag_id),
				str(tag[0])
			))+ '\n')
	at_buffer.seek(0)
	cursor.copy_from(at_buffer, "temp_assignedtag", sep='|', columns=("t_fk_tag_id", "t_fk_url_id"))

	# Merge Temp_assignedtag into Assignedtag
	print("{}Merging tags for {}".format(deltatime(), category))
	cursor.execute("""
		INSERT INTO Assignedtag (fk_tag_id, fk_url_id)
			SELECT t_fk_tag_id, t_fk_url_id FROM Temp_assignedtag
			ON CONFLICT DO NOTHING;
	""")
	cursor.execute("DELETE FROM Temp_assignedtag;")

# Drop temporary url table
print("{}Dropping temporary tables".format(deltatime()))
cursor.execute("DROP TABLE IF EXISTS Temp_url;")
#cursor.execute("DROP TABLE IF EXISTS Temp_assignedtag;")

# Commit changes and close connection
print("{}Committing changes and closing the connection".format(deltatime()))
conn.commit()
conn.close()
