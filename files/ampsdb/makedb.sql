-- ampsdb schema

CREATE DATABASE ampsdb;

\c ampsdb

DROP TABLE IF EXISTS Ampsuser CASCADE;
CREATE TABLE AmpsUser (
	ampsuser_id SERIAL PRIMARY KEY,
	ampsuser_name TEXT NOT NULL UNIQUE,
	ampsuser_password CHAR(60) NOT NULL
);

DROP TABLE IF EXISTS List CASCADE;
CREATE TABLE List (
	list_id SERIAL PRIMARY KEY,
	list_name TEXT NOT NULL,
	list_priority INTEGER NOT NULL
);

DROP TABLE IF EXISTS Url CASCADE;
CREATE TABLE Url (
	url_id SERIAL PRIMARY KEY,
	url_text TEXT NOT NULL UNIQUE,
	url_reason TEXT NOT NULL,
	fk_list_id INTEGER NOT NULL,
	FOREIGN KEY (fk_list_id) REFERENCES List(list_id)
);

DROP TABLE IF EXISTS Tag CASCADE;
CREATE TABLE Tag (
	tag_id SERIAL PRIMARY KEY,
	tag_name TEXT NOT NULL,
	tag_color CHAR(6) NOT NULL DEFAULT '5A5A5A'
);

DROP TABLE IF EXISTS Assignedtag CASCADE;
CREATE TABLE Assignedtag (
	assignedtag_id SERIAL PRIMARY KEY,
	fk_tag_id INTEGER NOT NULL,
	fk_url_id INTEGER NOT NULL,
	FOREIGN KEY (fk_tag_id) REFERENCES Tag(tag_id),
	FOREIGN KEY (fk_url_id) REFERENCES Url(url_id)
);

DROP TABLE IF EXISTS Event CASCADE;
CREATE TABLE Event (
	event_id SERIAL PRIMARY KEY,
	event_type TEXT NOT NULL,
	event_text TEXT NOT NULL,
	event_timestamp TIMESTAMPTZ NOT NULL
);

-- Create the admin user
-- IMPORTANT: change the password immediately

INSERT INTO AmpsUser (ampsuser_name, ampsuser_password)
VALUES ('admin', '$2a$12$cbgyrl.w3X.IUzlo6YHlLOikMSRON.y9rJb1QFxTdP3lZ/9CcBZIi');

-- Make default lists

INSERT INTO List (list_name, list_priority)
VALUES
	('default', 0),
	('blacklist', 1),
	('whitelist', -1),
	('superblacklist', 2),
	('superwhitelist', -2);

-- Make default tags

INSERT INTO Tag (tag_name)
VALUES
	('automatic')
	('manual')