#!/bin/bash

#SKRIPTS AUTOMATISKAI DATUBAZES LEJUPIELADEI PRIEKS SQUIDGUARD
#UN AUTOMATISKAI PARKOMPILACIJAI

DATUBAZE=/var/squidGuard/blacklists #Blacklist lokacija
TEMP=/var/squidGuard/temporary #Pagaidu datubazes fodlera lokacija
SAITE=http://www.shallalist.de/Downloads/shallalist.tar.gz #URL no kura tiek nemta datubaze
SATURS=/var/squidGuard/blacklists/BL #Pati datubaze

function apply_access_rules(){ #Funkcija prieks pieejas tiesibu mainas, lai squid var pieklut folderiem.
	echo "Mainu pieejas tiesibas!"
	chown -R squid:squid /var/squidGuard/blacklists/*
	chown -R squid:squid /var/log/squidGuard/squidGuard.log
	find /var/squidGuard/blacklists -type f | xargs chmod 644
	find /var/squidGuard/blacklists -type d | xargs chmod 755
	echo "Pieejas tiesibas nomainitas!"
}

function restart_services(){ #Funkcija datubazes parkompilacijas uz .db formatu un visus servisu restartam
	echo "Tiek sakts parkompilacijas un restarta process.."
	squidGuard -b -d -C all #Si linija atbildiga par parkompilaciju
	echo "Restarteju squid"
	systemctl restart squid
	echo "Restarteju squidGuard"
	systemctl restart squidGuard
	echo "Visi resursi ir restarteti un parkompileti!"
}
function unpack_resource(){ #Funkcija prieks jaunas datubazes lejupielades arhiva formata un ta izpakosanai
	echo "Atveru ${1} folderi"
	cd $1
	echo "Saku lejupieladi no ${2}"
	wget $2

	if [ $? -ne 0 ] #Parbaude vai viss notika gludi, ja nav, tad beidz darbu - japarliecinas vai URL darbojas
	then
		echo "${2} lejupielade neizdevas!"
		exit 0
	else
		echo "Datubaze veiksmigi lejupieladeta!"
	fi

	echo "Izpakoju tar.gz failu"
	tar -zxf shallalist.tar.gz #Parbaude vai arhivs ir veiksmigi izpakots, ja nav, tad beidz darbu
	if [ $? -ne 0 ]
	then
		echo "Arhiva izpakosana neizdevas!"
		exit 0
	else
		echo "Arhivs veiksmigi izpakots!"
	fi

	rm shallalist.tar.gz #Pec izpakosanas izdzes arhivu, lai neaiznem servera atminu
	echo "Viss veiksmigi izpakots!"
}

function remove_temp_if_exists(){ #Ja skripts tiek paragri apturets, tad netiek paspets izdzest temporary fodleri
				  #Tadel tiek veikta parbaude vai vins eksiste, ja eksiste - izdzest
	if [ -d $TEMP ] ; then 
		rm -r $TEMP
	fi
}

remove_temp_if_exists #Pasa skripta sakuma veicam parbaudi uz pre-mature apturesanas pazimem

#Parbaude vai blacklists folderis eksiste, ja neeksiste, tad tiek izveidots. Tiek pienemts, ka datubaze neeksiste, tadel tiek lejupieladeta ari pati datubaze, iedotas visas nepieciesamas tiesibas squidam, restarteti un parkompileti visi resursi un 
#programma beidz darbu
if [ ! -d /var/squidGuard/blacklists/ ]; then  
	echo "Neeksiste blacklist folderis - veidoju jaunu!"
	mkdir /var/squidGuard/blacklists
	unpack_resource $DATUBAZE $SAITE
	apply_access_rules
	restart_services
	echo "Darbs pabeigts, beidzu darbu"
	exit 0
fi
#Parbaude vai eksiste pati datubaze, neeksistesanas gadijuma tiek izveidota datubaze un programma beidz darbu
if [ ! -d /var/squidGuard/blacklists/BL/ ]; then
	echo "Datubaze neeksiste - tiek veidota jauna!"
	unpack_resource $DATUBAZE $SAITE
	apply_access_rules
	restart_services
	echo "Darbs pabeigts, beidzu darbu"
	exit 0
fi

mkdir $TEMP #Tiek izveidots temporary folderis

#Parbaude vai veiksmigi tika izveidots folderis
if [ $? -ne 0 ] 
then
	echo "${TEMP} Neizdevas izveidot!"
	exit 0
else
	echo "${TEMP} veiksmigi izveidots!"
fi 

#Temporary folderi tiek izpakota visa jauna datubaze
unpack_resource $TEMP $SAITE 

cd $SATURS
SKAITITAJS=0 #Mainigais, kurs tiks inkrementets balstoties uz parseto folderu skaitu
KOPEJAIS_SKAITS=0 #Mainigais, kurs satures kopeju folderu skaitu

#Nebutiska funkcija, kura tikai saskaita cik kopeji folderu eksiste
for FOLDERIS in `ls $SATURS`
do
	KOPEJAIS_SKAITS=$[ $KOPEJAIS_SKAITS+1 ]
done

#For funkcija, kura pielietojot cat ar sort|uniq parametriem izveido jaunu .tmp ar kura palidzibu parraksta
#veco failu
# cat     jaunais        vecais     sakarto   unikalais_ieraksts  ieks    jaunais.temporary
# cat (jaunaisFails) (vecaisFails) | sort |          uniq          >     (jaunaisTempFails)
for FAILS in `ls $SATURS`
do
	SKAITITAJS=$[ $SKAITITAJS+1 ] #Inkremente par +1
	#Parraksta domena failus
	if [ -f /var/squidGuard/blacklists/BL/$FAILS/domains ] #Parliecinas vai fails eksiste
	then
		cat /var/squidGuard/temporary/BL/$FAILS/domains /var/squidGuard/blacklists/BL/$FAILS/domains | sort | uniq > /var/squidGuard/blacklists/BL/$FAILS/domains.tmp  #Izveido temporary failu
		#Parraksta veco failu ar jauno temporary failu
		mv -f /var/squidGuard/blacklists/BL/$FAILS/domains.tmp /var/squidGuard/blacklists/BL/$FAILS/domains 
		#Izvada pazinojumu: Cik atjauninati / No cik un kads folderis
		echo "${SKAITITAJS}/${KOPEJAIS_SKAITS}: Atjauninats: ${FAILS}/domains" 
	fi
	#Parraksta url failus
	if [ -f /var/squidGuard/blacklists/BL/$FAILS/urls ]
	then
		cat /var/squidGuard/temporary/BL/$FAILS/urls /var/squidGuard/blacklists/BL/$FAILS/urls | sort | uniq > /var/squidGuard/blacklists/BL/$FAILS/urls.tmp
		mv -f /var/squidGuard/blacklists/BL/$FAILS/urls.tmp /var/squidGuard/blacklists/BL/$FAILS/urls
		echo "${SKAITITAJS}/${KOPEJAIS_SKAITS}:Atjauninats: ${FAILS}/urls"
	fi
done
echo "${SKAITITAJS} Faili atjauninati!"

apply_access_rules #Visiem atjauninatajiem failiem atsvaidzina pieejas tiesibas

restart_services #Visi servisi tiek restarteti

remove_temp_if_exists #Izdzes temporary folderi, lai lieki neaiznem vietu uz diska

echo "Datubaze veiksmigi atjauninata! Beidzu darbu" 
exit 0 #Programma beidz darbu


