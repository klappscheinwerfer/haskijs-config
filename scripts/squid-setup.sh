#!/bin/bash

# Install packages

apt-get install squid krb5-user -y
#libkrb53 libsasl2-modules-gssapi-mit libsasl2-modules ldap-utils

# Copy config files

cp ./files/squid.conf /etc/squid/squid.conf
cp ./files/krb5.conf /etc/krb5.conf

# Run services

systemctl enable squid
systemctl start squid
