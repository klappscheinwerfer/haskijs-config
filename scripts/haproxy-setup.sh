#!/bin/bash

# Install packages

apt-get install haproxy keepalived

# Copy config files
# NOTE: Select if you want haproxy to run as master or backup
# NOTE: Edit haproxy confing files (e.g. change IP)

cp ./files/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg
cp ./files/haproxy/keepalived.conf.master /etc/keepalived/keepalived.conf # Master
#cp ./files/haproxy/keepalived.conf.backup /etc/keepalived/keepalived.conf # Backup

# Start services

systemctl restart haproxy
systemctl restart keepalived
