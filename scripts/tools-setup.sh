#!/bin/bash

# Install packages

apt-get install git vim -y

# Remove unecessary packages

apt-get remove gnome-games -y
apt-get autoremove -y
