#!/bin/bash

# Install packages

apt install postgresql postgresql-client

apt install python3-pip libpq-dev
pip3 install psycopg2

# Create user and database

id -u pguser || adduser pguser

su -l postgres << "__END__"
createuser --pwprompt pguser
createdb -O pguser ampsdb
__END__

# Create tables

cat ./files/ampsdb/makedb.sql | psql -h localhost -d ampsdb -U pguser

# Copy scripts

cp ./files/ampsdb/ampsdb_get.py /usr/bin/ampsdb_get.py
cp ./files/ampsdb/ampsdb_put.py /usr/bin/ampsdb_put.py

# Add crontab

crontab ./files/ampsdb/cron
